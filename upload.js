let fs = require("fs");
let express = require('express'); 
let app = express();
let morgan = require('morgan'); // log requests to the console (express4)
let favicon = require('serve-favicon');

let port = process.env.PORT || 8080;

// configuration =================
app.use(morgan('dev'));                              // log every request to the console
app.use(favicon(__dirname + '/public/favicon.ico')); // set the site icon
app.use(express.static(__dirname + '/public'));      // set the static files location

app.post('/api/upload', (req, res) => { // for http POST to url http://localhost:8080/api/uload
   let fbytes = req.headers["content-length"];
   let fname = req.headers["x_filename"];

   let upbytes = 0;

   newfile = fs.createWriteStream("copy."+fname);

   req.on('data', stuff => {
     upbytes += stuff.length;
     let progress = (upbytes / fbytes) * 100;
     console.log("progress: " + parseInt(progress, 10) + "%\n");
     let good = newfile.write(stuff);
     if(!good) {
       console.log("Pause");
       req.pause();
     }
   });
   newfile.on('drain', () => {
     req.resume();
     console.log("Resume");
   });
   req.on('end', stuff => {
     res.end("Done");
     console.log("Uploaded");
     newfile.end();
   });
});

app.get('/', function(req, res) {
   // load the single view file (angular will handle the page changes on the front-end)
   res.redirect('upload.html');
});

let server = app.listen(port);
server.on('listening', function() { console.log('Listening to ', port); });
server.on('error', err => console.log(err));
