let http = require("http");
let fs = require("fs");

let port = process.env.PORT || 8080;

let server = http.createServer((req, res) => {
   let file = fs.createWriteStream("trump-copy2.jpg");
   req.pipe(file);
   req.on('end', stuff => {
     res.end("\nuploaded\n");
     file.end();
   });
});

server.listen(port);
server.on('listening', function() { console.log('Listening to ', port); });
server.on('error', err => console.log(err));
