let http = require("http");
let fs = require("fs");

let port = process.env.PORT || 8080;

let server = http.createServer((req, res) => {
   let newfile = fs.createWriteStream("trump-copy3.jpg");
   
   let fbytes = req.headers["content-length"];
   let upbytes = 0;

   req.on('data', stuff => {
     upbytes += stuff.length;
     let progress = (upbytes / fbytes) * 100;
     res.write("progress: " + parseInt(progress, 10) + "%\n");
     console.log("progress: " + parseInt(progress, 10) + "%\n");
     let good = newfile.write(stuff);
     if(!good) {
       console.log("Pause");
       req.pause();
     }
   });
   
   newfile.on('drain', () => {
     req.resume();
     console.log("Resume");
   });

   req.on('end', stuff => {
     res.end('Uploaded');
     console.log("Uploaded");
     newfile.end();
   });
});

server.listen(port);
server.on('listening', function() { console.log('Listening to ', port); });
server.on('error', err => console.log(err));
