let http = require("http");
let fs = require("fs");

let port = process.env.PORT || 8080;

let server = http.createServer((req, res) => {
   res.writeHead(200);
   req.pipe(res);
});

server.listen(port);
server.on('listening', function() { console.log('Listening to ', port); });
server.on('error', err => console.log(err));
