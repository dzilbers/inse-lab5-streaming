let http = require("http");
let fs = require("fs");

let port = process.env.PORT || 8080;

let server = http.createServer((req, res) => {
   res.writeHead(200);
   req.on('data', stuff => res.write(stuff.toString()));
   req.on('end', stuff => res.end());
});

server.listen(port);
server.on('listening', function() { console.log('Listening to ', port); });
server.on('error', err => console.log(err));
