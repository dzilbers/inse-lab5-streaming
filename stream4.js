let http = require("http");
let fs = require("fs");

let port = process.env.PORT || 8080;

let server = http.createServer((req, res) => {
   res.writeHead(200);
   req.on('data', stuff => {
     let good = res.write(stuff.toString());
     if (!good) req.pause();
   });
   res.on('drain', stuff => req.resume());
   req.on('end', stuff => res.end());
});

server.listen(port);
server.on('listening', function() { console.log('Listening to ', port); });
server.on('error', err => console.log(err));
